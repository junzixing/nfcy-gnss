# 怒飞垂云-GNSS模块开源硬件

## 介绍
怒飞垂云-GNSS模块硬件设计文件

1. PCB采用Altium Designer绘制，提供整个设计工程和焊接表；
2. 提供外壳STEP文件；
3. 无开源协议，你可以随意修改并商用这款硬件。

## 硬件配置
1. GNSS模块：u-blox M9N，支持北斗、GPS、格洛纳斯和伽利略四系统并发处理，但是我们屏蔽了经常掉链子的伽利略系统，改为三系统并发处理，定位更稳健！
2. 陶瓷天线：太盟PA025AZ009，定制款天线，支持北斗、GPS、格洛纳斯三系统。根据太盟的回复，25*25*4尺寸的陶瓷天线无法做到完美的四系统兼容，因此砍掉了国内不常用的伽利略系统支持，如果强行支持伽利略系统，会导致其他三系统的效果变差
3. 主处理器：STM32F412，Cortex- M4内核
4. 磁罗盘：IST8310
5. RGB LED：7个高亮度RGB LED环绕排列
6. 对外接口：CAN总线，UAVCAN协议（又名DroneCAN协议）

![](./图片/PCB正面视图.png)

## RGB LED效果
![](./图片/RGB_LED.gif)

## 测试环境
### 正北方向150米和正东方向100米处都是20层以上的高楼
![](./图片/GNSS_TEST.jpg)

## 测试效果
### 在此遮挡环境下轻松28颗星，空旷地带则最高观测到过32颗星
![](./图片/GNSS_FIX.png)

## 固件移植
### 固件源代码：<a href="https://github.com/nufeichuiyun/ardupilot" target="_blank">https://github.com/nufeichuiyun/ardupilot</a>，分支名：My_AP_Periph-1.5.1 

### 编译好的固件：在本仓库“固件”文件夹下

### 固件编译视频教程：<a href="https://study.163.com/course/introduction/1209568864.htm?share=1&shareId=1448054983" target="_blank">https://study.163.com/course/introduction/1209568864.htm?share=1&shareId=1448054983</a> 课时20

## 联系方式：QQ 3500985084（飞控固件开发、硬件设计、总体方案设计等等）

## 怒飞垂云官网：
<a href="http://www.nufeichuiyun.com/" target="_blank">http://www.nufeichuiyun.com/</a>

## 《无人机固件开发教程》视频教程链接（含此GNSS模块固件移植教程）：
<a href="https://study.163.com/course/introduction/1209568864.htm?share=1&shareId=1448054983" target="_blank">https://study.163.com/course/introduction/1209568864.htm?share=1&shareId=1448054983</a>

## 怒飞垂云：怒而飞，其翼若垂天之云！

![](./图片/怒飞垂云-公众号.jpg)